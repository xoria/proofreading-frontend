const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const DIR_ROOT = path.resolve(__dirname, "..");

module.exports = genConfig;

function genConfig(argv, mode) {
  const config = require("../config/" + mode.name);
  return {
    mode: mode.name,
    entry: {
      main: path.join(DIR_ROOT, "src", "index.jsx"),
    },
    output: {
      filename: "[name].[hash].js",
      publicPath: "/"
    },
    resolve: {
      modules: [DIR_ROOT, path.resolve(DIR_ROOT, "node_modules")],
      extensions: [".js", ".mjs", ".jsx"],
      alias: {
        "!": DIR_ROOT,
      }
    },
    plugins: [
      new webpack.ProvidePlugin({
        Preact: "preact", // for @babel/plugin-transform-react-jsx
      }),
      new HtmlWebpackPlugin({
        template: "index.html",
        filename: "index.html",
        hash: true,
        xhtml: true,
      }),
      new MiniCssExtractPlugin({
        filename: "[name].[hash].css",
        chunkFilename: "[id].css",
      }),
      new webpack.DefinePlugin({
        "process.env.PRODUCTION": JSON.stringify(mode.production),
        "process.env.CONFIG": JSON.stringify(config),
      }),
    ],
    module: {
      rules: [
        {
          test: /\.(jsx|m?js)$/,
          exclude: /node_modules/,
          use: [
            {
              loader: "babel-loader",
              options: require("../.babelrc"),
            },
          ]
        },
        {
          test: /\.s?css$/,
          use: [
            "style-loader", // creates style nodes from JS strings
            "css-loader", // translates CSS into CommonJS
            "postcss-loader", // see postcss.config.js
            "sass-loader", // compiles Sass to CSS, using Node Sass by default
          ]
        },
        // Fonts
        {
          test: /\.svg$/,
          use: [
            {
              loader: "url-loader",
              options: {limit: 5000, mimetype: "image/svg", name: "public/fonts/[name].[ext]",}
            }
          ],
        },
        {
          test: /\.woff$/,
          use: [
            {
              loader: "url-loader",
              options: {limit: 5000, mimetype: "application/font-woff", name: "public/fonts/[name].[ext]",}
            }
          ],
        },
        {
          test: /\.woff2$/,
          use: [
            {
              loader: "url-loader",
              options: {limit: 5000, mimetype: "application/font-woff2", name: "public/fonts/[name].[ext]",}
            }
          ],
        },
        {
          test: /\.[ot]tf$/,
          use: [
            {
              loader: "url-loader",
              options: {limit: 5000, mimetype: "application/octet-stream", name: "public/fonts/[name].[ext]",}
            }
          ],
        },
        {
          test: /\.eot$/,
          use: [
            {
              loader: "url-loader",
              options: {limit: 5000, mimetype: "application/vnd.ms-fontobject", name: "public/fonts/[name].[ext]",}
            }
          ],
        },
      ]
    },
  };
}
