const _ = require("lodash");
const path = require("path");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const baseOptions = require("./_base");

const DIR_ROOT = path.resolve(__dirname, "..");
const DIR_TARGET = path.join(DIR_ROOT, "dist");

module.exports = genConfig;

function genConfig(argv, mode) {
  const options = _.defaultsDeep(
      {
        output: {
          path: DIR_TARGET,
          chunkFilename: "[name].[hash].bundle.min.js",
          filename: "[name].[hash].min.js"
        }
      },
      baseOptions(argv, mode)
  );
  options.plugins.unshift(new CleanWebpackPlugin(DIR_TARGET, {root: DIR_ROOT, watch: true}));
  // replace style-loader with MiniCssExtractPlugin
  _.find(options.module.rules, (rule) => rule.use[0] === "style-loader").use[0] = MiniCssExtractPlugin.loader;
  return options;
}
