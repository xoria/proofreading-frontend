const _ = require("lodash");
const path = require("path");
const CleanWebpackPlugin = require("clean-webpack-plugin");

const baseOptions = require("./_base");

const DIR_ROOT = path.resolve(__dirname, "..");
const DIR_TARGET = path.join(DIR_ROOT, ".tmp");

module.exports = genConfig;

function genConfig(argv, mode) {
  const options = _.defaultsDeep(
      {
        watch: true,
        watchOptions: {
          aggregateTimeout: 200,
          poll: 1000,
          ignored: /node_modules/,
        },
        output: {
          path: DIR_TARGET,
        },
        devtool: "source-map",
      },
      baseOptions(argv, mode)
  );
  options.plugins.unshift(new CleanWebpackPlugin(DIR_TARGET, {root: DIR_ROOT, exclude: ["public"], watch: true}));
  return options;
}
