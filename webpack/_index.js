const _ = require("lodash");

const MODES = [
  ["production", "prod"], // first entry => default
  ["development", "dev"],
];

module.exports = genConfig;

function genConfig(env, argv) {
  const mode = parseMode(env);
  return require("./" + mode.name)(argv, mode);
}

function parseMode(env = MODES[0][0]) {
  const result = {};
  let modeName = null;
  for (let mode of MODES) {
    const name = mode[0];
    if (typeof env === "string") {
      result[name] = mode.includes(env);
    } else if (typeof env.target === "string") {
      result[name] = mode.includes(env.target);
    } else {
      result[name] = _.some(mode, (m) => env[m]);
    }
    if (result[name]) { modeName = name; }
  }
  if (modeName === null) { throw new Error("Unknown webpack target."); }
  result.name = modeName;
  return result;
}
