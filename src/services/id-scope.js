export {nextId};

/*==================================================== Functions  ====================================================*/

function nextId(cache, idScope) {
  let id;
  do {
    id = (Math.random() * 2 ** 16) | 0;
  } while (cache.has(id));
  cache.add(id);
  const idString = id.toString(16).padStart(4, "0");
  return idString.charAt(0) + idScope + idString.substr(1);
}
