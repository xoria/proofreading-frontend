import {Node} from "prosemirror-model";
import schema from "../../constants/document-schema";

/*===================================================== Exports  =====================================================*/

export default class FallbackConnector {

  constructor() { this._state = null; }

  async start(authority) {
    this._authority = authority;
    authority.on("state:update", this._updateState);
  }

  get state() {
    if (this._state == null) {
      this._state = this._authority.getStateNow(this);
      if (this._state == null) { this._state = create(); }
    }
    return this._state;
  }

  _updateState = (data) => {
    this._state = {
      ...this._authority.getStateNow(),
      ...data,
    };
  };

}

/*==================================================== Functions  ====================================================*/

function create() {
  return {
    doc: Node.fromJSON(schema, {type: "doc", content: [{type: "paragraph"}]}), // keep in sync with server creation
    version: 0,
    inverseSteps: [],
    idScope: "000-0000-00000000-0",
    comments: [],
    suggestions: [],
    chat: [],
  };
}
