// TODO move into custom npm module
import {Plugin, PluginKey, TextSelection} from "prosemirror-state";
import {Mapping} from "prosemirror-transform";

const PLUGIN_KEY = new PluginKey("rebase");

/*===================================================== Classes  =====================================================*/

/**
 * A plugin that allows to splice steps into the cached history of some editor state. This can be used as a quite
 * generic plugin for collaborative editing features.
 */
class RebasePlugin extends Plugin {

  /**
   * Create a new instance of this plugin.
   *
   * @param {Number} [baseVersion = 0] The initial base version (see {@link History#constructor}).
   * @param {BiStep[]} [initialBiSteps = []] The initial cached steps (see {@link History#constructor}).
   */
  constructor(baseVersion = 0, initialBiSteps = []) {
    super({
      historyPreserveItems: true,
      key: PLUGIN_KEY,
      state: {
        init: () => new History(baseVersion, initialBiSteps),
        apply: transactionEvent,
      },
    });
  }

}

/**
 * Holds the history of steps and a base version number.
 * The document of the editor state that holds this a History instance is always considered to be the state after all
 * {@link History.biSteps}.
 */
class History {

  /**
   * @param {Number} baseVersion The version of the earliest available `doc`.
   * @param {BiStep[]} biSteps The local {@link BiStep}s that produced later versions of the `doc`.
   */
  constructor(baseVersion, biSteps) {
    this.baseVersion = baseVersion;
    this.biSteps = biSteps;
  }

  get maxVersion() { return this.baseVersion + this.biSteps.length; }

  expectContain(version) {
    if (version < this.baseVersion) {
      throw new Error("Cannot perform operation for any version earlier than available.");
    }
    if (version > this.maxVersion) {
      throw new Error("Cannot perform operation for any version later than available.");
    }
  }

  concat(biSteps) { return new History(this.baseVersion, this.biSteps.concat(biSteps)); }

}

/**
 * A bi-directional wrapper of some step.
 */
class BiStep {

  static fromStep(step, doc) { return new BiStep(step, step.invert(doc)); }

  static fromInverseStep(step, doc) { return new BiStep(step.invert(doc), step); }

  constructor(forward, backward) {
    this.forward = forward;
    this.backward = backward;
  }

}

/*==================================================== Functions  ====================================================*/

/*------------------------------------------------------- API  -------------------------------------------------------*/

/**
 * Get the attached state of this plugin.
 *
 * @param {EditorState} state The state to operate on.
 * @return {History} The plugin state of the provided editor state.
 */
function getState(state) { return PLUGIN_KEY.getState(state); }

/**
 * Create a new transaction that updates the {@link History} to drop all steps before the specified version. If applied,
 * no rebase can be performed to those earlier versions. This transaction is useful to free up memory.
 *
 * @param {EditorState} state The state to operate on.
 * @param {Number} beforeVersion The version to drop record of everything that happened earlier. This version must be
 *        within the range of the {@link History}.
 * @return {Transaction} The newly created transaction.
 */
function dropTransaction(state, beforeVersion) {
  const history = PLUGIN_KEY.getState(state);
  history.expectContain(beforeVersion);
  const transaction = state.tr;
  const diff = beforeVersion - history.baseVersion;
  if (diff <= 0) { return transaction; }
  transaction
      .setMeta(PLUGIN_KEY, new History(beforeVersion, history.biSteps.slice(diff)));
  return transaction;
}

/**
 * Create a new transaction that performs a rebase. If applied, the passed steps are applied at the specified base
 * version.
 *
 * @param {EditorState} state The state to operate on.
 * @param {Number} baseVersion The base version to splice in the steps. This version must be within the range of the
 *        {@link History}.
 * @param {Step[]} steps The steps to splice in.
 * @param {Boolean} [clearHistory = false] Whether to combine with a {@link dropTransaction} for the version after the
 *        passed steps. This may provide significant performance improvements over separate transactions.
 * @param {Boolean} [exportBiSteps = false] If set to true, the bi-directional wrappers of the new rebased history
 *        (including the passed steps) will be attached to the transaction meta as `"rebased:bi-steps"`.
 * @return {Transaction} The newly created transaction.
 *
 * @see rebaseSteps
 */
function rebaseTransaction(state, baseVersion, steps, {clearHistory = false, exportBiSteps = false}) {
  const history = PLUGIN_KEY.getState(state);
  history.expectContain(baseVersion);
  const versionDiff = baseVersion - history.baseVersion;
  const transaction = state.tr;
  const biStepsToRebase = history.biSteps.slice(versionDiff);
  let biStepsReport = rebaseSteps(biStepsToRebase, steps, transaction, !clearHistory || exportBiSteps);
  if (exportBiSteps) {
    transaction.setMeta("rebased:bi-steps", biStepsReport);
    if (clearHistory) { biStepsReport = biStepsReport.slice(steps.length); }
  }
  let nextHistory;
  if (clearHistory) {
    nextHistory = new History(baseVersion + steps.length, biStepsReport);
  } else {
    nextHistory = new History(history.baseVersion, history.biSteps.slice(0, versionDiff).concat(biStepsReport));
  }
  transaction
      .setMeta("rebased", biStepsToRebase.length) // tell prosemirror-history about that rebase
      .setMeta("addToHistory", false) // tell prosemirror-history not to rollback this transaction by undo
      .setMeta(PLUGIN_KEY, nextHistory);
  return transaction;
}

/*---------------------------------------------------- Utilities  ----------------------------------------------------*/

/**
 * Rebase some biSteps (A) to some steps (B). So the current step order `[..., biStepsA]` will be rebased to
 * `[..., stepsB, biStepsA]`. This is done by first applying the inverse steps of (A).
 *
 * @param {BiStep[]} biStepsA The biSteps to undo and redo after `stepsB`.
 * @param {Step[]} stepsB The steps to splice into the history.
 * @param {Transaction} transaction The transaction to register the operations to.
 * @param {Boolean} fullReport Whether to return the full rebased history `[biStepsB, biStepsA]` or just the second part
 *        of it. This is introduced to avoid the performance overhead to inverse all steps (B) when not used anyways.
 * @return {BiStep[]} The report according to `fullReport`.
 */
function rebaseSteps(biStepsA, stepsB, transaction, fullReport) {
  const report = [];
  // 1. Undo steps A:
  for (let i = biStepsA.length - 1; i >= 0; i--) { transaction.step(biStepsA[i].backward); }
  // 2. Apply steps B:
  const selection = transaction.selection;
  const stepMaps = [];
  for (let step of stepsB) {
    if (fullReport) { report.push(BiStep.fromStep(step, transaction.doc)); }
    transaction.step(step);
    stepMaps.push(step.getMap());
  }
  // map TextSelections with negative bias instead of default positive bias
  if (selection instanceof TextSelection) { mapTextSelectionNegBias(selection, new Mapping(stepMaps), transaction); }
  // 3. Redo steps A:
  let mapFrom = biStepsA.length;
  for (let hStep of biStepsA) {
    const mappedStep = hStep.forward.map(transaction.mapping.slice(mapFrom--));
    if (mappedStep == null) { continue; } // step got deleted simply by mapping (consider applied)
    const preStepDoc = transaction.doc;
    if (!transaction.maybeStep(mappedStep).failed) {
      transaction.mapping.setMirror(mapFrom, transaction.steps.length - 1);
      report.push(BiStep.fromStep(mappedStep, preStepDoc));
    }
  }
  return report;
}

function toBiSteps(transaction) {
  const historySteps = [];
  for (let i = 0; i < transaction.steps.length; i++) {
    const step = transaction.steps[i];
    historySteps.push(new BiStep(step, step.invert(transaction.docs[i])));
  }
  return historySteps;
}

function mapTextSelectionNegBias(textSelection, mapping, transaction) {
  const doc = transaction.doc;
  transaction.setSelection(
      TextSelection.between(
          transaction.doc.resolve(mapping.map(textSelection.head, -1)),
          transaction.doc.resolve(mapping.map(textSelection.anchor, -1)),
          -1
      )
  );
  transaction.setSelection(textSelection.map(doc, mapping));
  transaction.updated &= ~1;
}

function transactionEvent(transaction, history) {
  // if transaction has defined state within meta (e.g. transactions created by this plugin), return it
  const newState = transaction.getMeta(PLUGIN_KEY);
  if (newState != null) { return newState; }
  // if no doc changes, do not update the state
  if (!transaction.docChanged) { return history; }
  // append biSteps from transaction to history
  return history.concat(toBiSteps(transaction));
}

/*===================================================== Exports  =====================================================*/

export {
  BiStep,

  getState,
  dropTransaction, rebaseTransaction,
};

export default RebasePlugin;
