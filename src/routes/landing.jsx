export default PageLanding;

/*==================================================== Component  ====================================================*/

function PageLanding() {
  return (
      <article id="landing-page">
        <div className="empty">
          <div className="empty-icon"><i className="fas fa-3x fa-highlighter"></i></div>
          <p className="empty-title h5">Collaborative Proofreading</p>
          <p className="empty-subtitle">No registration required.</p>
          <div className="empty-action">
            <a href="/document" className="btn btn-lg btn-primary">Get started</a>
          </div>
        </div>
        <div className={"columns"}>
          <div className={"column col-4 col-md-6 col-xs-12"}>
            <div className={"empty empty-transparent"}>
              <p className={"empty-icon"}>
                <i className={"fas fa-2x fa-user-secret"}/>
              </p>
              <p className="empty-title h5">Privacy first</p>
              <p className="empty-subtitle">
                Secure connection, distraction-free offline mode and fine-grained access control.
              </p>
            </div>
          </div>
          <div className={"column col-4 col-md-6 col-xs-12"}>
            <div className={"empty empty-transparent"}>
              <p className={"empty-icon"}>
                <i className={"fas fa-2x fa-plug"}/>
              </p>
              <p className="empty-title h5">Simple yet Powerful</p>
              <p className="empty-subtitle">
                With a minimal interface you control only the relevant aspects of your documents.
              </p>
            </div>
          </div>
          <div className={"column col-4 col-md-12"}>
            <div className={"empty empty-transparent"}>
              <p className={"empty-icon"}>
                <i className={"fas fa-2x fa-business-time"}/>
              </p>
              <p className="empty-title h5">No wasted time</p>
              <p className="empty-subtitle">
                The Proofreaders can see each others comments. This saves time for all involved people.
              </p>
            </div>
          </div>
        </div>
        <div className={"empty"}>
          Some detailed description and workflow illustrations to follow...
        </div>
      </article>
  );
}
