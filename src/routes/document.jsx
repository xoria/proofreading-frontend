import {PropTypes} from "prop-types";
import getSession from "../api";
import {route} from "preact-router";
import DocumentContainer from "../components/document.container";

/*===================================================== Exports  =====================================================*/

export default PageDocument;

PageDocument.propTypes = {id: PropTypes.string};

/*==================================================== Component  ====================================================*/

function PageDocument({id}) {
  if (id === null) {
    const session = getSession();
    session
        .query("document:create")
        .then((result) => route("/document/_/" + result.documentId, true));
    return;
  }
  return <DocumentContainer documentId={id}/>;
}
