import {initial as documentSettingsInitial, KEY as documentSettingsKey} from "./actions/document-settings";

/*===================================================== Exports  =====================================================*/

export default {
  [documentSettingsKey]: documentSettingsInitial(),
};
