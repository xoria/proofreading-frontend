import {createStore} from "redux";
import initial from "./initial";
import actions from "./actions";

/*===================================================== Exports  =====================================================*/

export default init();

/*==================================================== Functions  ====================================================*/

function init() {
  return createStore(
      (state, action) => {
        if (!actions.hasOwnProperty(action.type)) {
          // eslint-disable-next-line no-console
          process.env.PRODUCTION || console.warn("Store action '" + action.type + "' does not exist.");
          return state;
        }
        return actions[action.type](state, action.payload, action.error, action.meta);
      },
      initial,
  );
}
