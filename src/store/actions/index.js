import {ACTIONS as documentSettings} from "./document-settings";

/*===================================================== Exports  =====================================================*/

export default {
  ...documentSettings,
};
