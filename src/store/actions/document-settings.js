import {DOCUMENT_SETTINGS_PUT, DOCUMENT_SETTINGS_REMOVE} from "../action-types";

const STORAGE_KEY = "document-settings";
const KEY = "documentSettings";

/*===================================================== Exports  =====================================================*/

export const ACTIONS = {
  [DOCUMENT_SETTINGS_PUT]: put,
  [DOCUMENT_SETTINGS_REMOVE]: remove,
};

export {KEY, read as initial};

/*==================================================== Functions  ====================================================*/

function put(state, {documentId, settings}) {
  const _state = {
    ...state[KEY],
    [documentId]: settings,
  };
  persist(_state);
  return {...state, [KEY]: _state};
}

function remove(state, {documentId}) {
  const _state = {}, current = state[KEY];
  for (let key in current) { if (current.hasOwnProperty(key) && key !== documentId) { _state[key] = current[key]; } }
  persist(_state);
  return {...state, [KEY]: _state};
}

function persist(settingsObject) {
  const storeObject = {};
  for (let key in settingsObject) {
    if (settingsObject.hasOwnProperty(key)) {
      const settings = settingsObject[key];
      storeObject[key] = [settings.syncOnline, settings.syncOffline];
    }
  }
  localStorage.setItem(STORAGE_KEY, JSON.stringify(storeObject));
}

function read() {
  const storeString = localStorage.getItem(STORAGE_KEY);
  if (storeString == null) { return {}; }
  const storeObject = JSON.parse(storeString);
  const settingsObject = {};
  for (let key in storeObject) {
    if (storeObject.hasOwnProperty(key)) {
      const current = storeObject[key];
      settingsObject[key] = {syncOnline: current[0], syncOffline: current[1]};
    }
  }
  return settingsObject;
}
