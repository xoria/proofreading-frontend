import config from "./config";

let session = null;
let nextQueryId = 0;

/*===================================================== Classes  =====================================================*/

class Session {

  constructor() {
    this._socket = new WebSocket(config.API_URL);
    this._listeners = {};
    this._queryListeners = {};
    this._socket.onmessage = this._receive.bind(this);
    this.readyPromise = new Promise((resolve) => { this._socket.onopen = () => resolve(); });
  }

  async query(type, payload) {
    await this.readyPromise;
    const queryId = nextQueryId++;
    return await new Promise((resolve, reject) => {
      this._queryListeners[queryId] = (err, result) => { if (err != null) { reject(err); } else { resolve(result); } };
      if (payload === void 0) {
        this._socket.send(`[1,${queryId},"${type}"]`);
      } else {
        this._socket.send(`[1,${queryId},"${type}",${JSON.stringify(payload)}]`);
      }
    });
  }

  async notify(type, payload) {
    await this.readyPromise;
    if (payload === void 0) {
      this._socket.send(`[0,"${type}"]`);
    } else {
      this._socket.send(`[0,"${type}",${JSON.stringify(payload)}]`);
    }
  }

  listen(type, fn) {
    if (this._listeners.hasOwnProperty(type)) { // eslint-disable-next-line no-console
      console.warn("Overwriting socket type handler", type);
    }
    this._listeners[type] = fn;
  }

  mute(type, fn) {
    if (fn === void 0 || this._listeners[type] === fn) { Reflect.deleteProperty(this._listeners, type); }
  }

  _receive({data}) {
    const [isQuery, isError, id, payload] = JSON.parse(data);
    if (isQuery) {
      if (isError) { this._queryListeners[id](payload); } else { this._queryListeners[id](null, payload); }
      Reflect.deleteProperty(this._queryListeners, "_id");
    } else {
      if (!this._listeners.hasOwnProperty(id)) { // eslint-disable-next-line no-console
        return console.warn("Socket has no such type handler", id);
      }
      this._listeners[id](payload);
    }
  }

}

/*==================================================== Functions  ====================================================*/

function getSession() {
  if (session === null) { session = new Session(); }
  return session;
}

/*===================================================== Exports  =====================================================*/

export default getSession;
