import "../styles/index.scss";

import {render} from "preact";
import {Provider} from "preact-redux";
import MyRouter from "./router";
import ErrorBoundary from "./components/error-boundary";
import Navigation from "./components/navigation";
import initAPI from "./api";
import store from "./store";

/*================================================ Initial execution  ================================================*/

render((
    <ErrorBoundary>
      <Provider store={store}>
        <header>
          <Navigation/>
        </header>
      </Provider>
    </ErrorBoundary>
), null, document.getElementById("mount-header"));

render((
    <ErrorBoundary>
      <Provider store={store}>
        <div className="container">
          <MyRouter/>
        </div>
      </Provider>
    </ErrorBoundary>
), null, document.getElementById("mount-body"));

setTimeout(() => {
  const session = initAPI();
  // eslint-disable-next-line no-console
  session.listen("error", console.error);
});
