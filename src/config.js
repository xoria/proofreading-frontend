const config = process.env.CONFIG;

if (!process.env.PRODUCTION) { // eslint-disable-next-line
  console.log("Configuration:", config);
}

export default config;
