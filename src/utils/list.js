export {removeFromList};

/*==================================================== Functions  ====================================================*/

function removeFromList(list, item) {
  const idx = list.indexOf(item);
  if (~idx) {
    list.splice(idx, 1);
    return true;
  }
  return false;
}
