// This file needs to be kept the same as within backend src/constants/document-schema.mjs
import {schema} from "prosemirror-schema-basic";

/*===================================================== Exports  =====================================================*/

export default schema;
