import {redo, undo} from "prosemirror-history";
import schema from "./document-schema";
import {
  chainCommands,
  createParagraphNear,
  deleteSelection,
  joinBackward,
  joinForward,
  liftEmptyBlock,
  selectNodeBackward,
  selectNodeForward,
  splitBlock
} from "prosemirror-commands";

const backspace = chainCommands(deleteSelection, joinBackward, selectNodeBackward);
const del = chainCommands(deleteSelection, joinForward, selectNodeForward);
const enter = chainCommands(createParagraphNear, liftEmptyBlock, splitBlock);

/*===================================================== Exports  =====================================================*/

export default {
  "Mod-z": undo,
  "Mod-y": redo, "Mod-Shift-z": redo,
  Backspace: backspace,
  Delete: del,
  Enter: enter,
  "Shift-Enter": pmCmdAddNewline,
};

/*==================================================== Functions  ====================================================*/

function pmCmdAddNewline(state, dispatch) {
  if (dispatch) { dispatch(state.tr.replaceSelectionWith(schema.node("hard_break")).scrollIntoView()); }
  return true;
}

