import {Component} from "preact";
import PropTypes from "prop-types";
import EditorControlsContainer from "./editor-controls.container";
import DocumentEditor from "./document-editor";
import DocumentAnnotations from "./document-annotations";
import Authority from "../services/document-authority";
import schema from "../constants/document-schema";
import LocalStorageConnector from "../services/document-connectors/local-storage";
import APIConnector from "../services/document-connectors/api";
import FallbackConnector from "../services/document-connectors/fallback";
import {connect} from "preact-redux";
import {putDocumentSettings} from "../store/action-creators";

/*==================================================== Container  ====================================================*/

@connect(mapStateToProps, mapDispatchToProps)
class DocumentContainer extends Component {

  static propTypes = {
    documentId: PropTypes.string.isRequired,
    syncOnline: PropTypes.bool.isRequired,
    syncOffline: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {authority: null};
  }

  async componentDidMount() {
    const {documentId, syncOffline, syncOnline} = this.props;
    this.syncOfflineConnector = new LocalStorageConnector(documentId);
    this.syncOnlineConnector = new APIConnector(documentId);
    if (!syncOffline) { this.syncOfflineConnector.disable(); }
    if (!syncOnline) { this.syncOnlineConnector.disable(); }
    const connectors = [
      this.syncOfflineConnector,
      this.syncOnlineConnector,
      new FallbackConnector(),
    ];
    const authority = new Authority(schema, connectors);
    this.setState({authority});
  }

  componentDidUpdate(prevProps) {
    const {syncOffline, syncOnline} = this.props;
    if (syncOffline !== prevProps.syncOffline) {
      if (syncOffline) { this.syncOfflineConnector.enable(); } else { this.syncOfflineConnector.disable(); }
    }
    if (syncOnline !== prevProps.syncOnline) {
      if (syncOnline) { this.syncOnlineConnector.enable(); } else { this.syncOnlineConnector.disable(); }
    }
  }

  render({documentId, ...props}, {authority}) {
    if (authority == null) {
      return <article id="document" data-document-id={documentId} {...props}>Please wait...</article>;
    }
    return (
        <article id="document" data-document-id={documentId} {...props}>
          <div className="columns">
            <div className="column col-8 col-md-12">
              <EditorControlsContainer className={"mb-4"} documentId={documentId}/>
              <DocumentEditor className={"panel"} authority={authority}/>
            </div>
            <div className="column col-4 col-md-12">
              <DocumentAnnotations authority={authority}/>
            </div>
          </div>
        </article>
    );
  }

}

export default DocumentContainer;

/*==================================================== Functions  ====================================================*/

function mapStateToProps(state, {documentId}) {
  const settings = state.documentSettings[documentId];
  return {
    syncOnline: settings == null ? true : settings.syncOnline,
    syncOffline: settings == null ? false : settings.syncOffline,
  };
}

function mapDispatchToProps(dispatch, {documentId}) {
  return {
    setActive(settings) { dispatch(putDocumentSettings(documentId, settings)); },
  };
}
