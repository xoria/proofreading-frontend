import {Component} from "preact";
import PropTypes from "prop-types";

/*==================================================== Component  ====================================================*/

export default class Comment extends Component {

  static propTypes = {
    comment: PropTypes.any.isRequired,
  };

  render({className, comment, ...props}) {
    const classNames = ["tile", className || ""].join(" ");
    return (
        <div className={classNames} {...props}>
          <div className="tile-icon">
            <figure className="avatar">
              <img src="https://www.gravatar.com/avatar/bfd1c0a0804aa69dcc8c981e42dab725?d=wavatar" alt="Brosenne"/>
            </figure>
          </div>
          <div className="tile-content">
            <p className="tile-title text-bold">
              {comment.from}
              <span className={"float-right ml-2"}><i className={"fas fa-lg fa-highlighter"}/></span>
            </p>
            {
              comment.tags.length > 0 &&
              <p className={"chips"}>
                {comment.tags.map((tag, i) => <span key={i} className="chip">{tag}</span>)}
              </p>
            }
            <p className="tile-subtitle">{comment.body}</p>
          </div>
        </div>
    );
  }

}
