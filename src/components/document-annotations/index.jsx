import "./style.scss";

import PropTypes from "prop-types";
import {Component} from "preact";
import NewCommentForm from "../new-comment-form";
import Comment from "../comment";

/*==================================================== Component  ====================================================*/

export default class DocumentAnnotations extends Component {

  static propTypes = {
    authority: PropTypes.any.isRequired,
  };

  state = {
    comments: [],
    suggestions: [],
    chat: [],
  };

  async componentDidMount() {
    const authority = this.props.authority;
    this._unmount = authority.on("state:update", ({comments, suggestions, chat}) => {
      if (comments != null) { this.setState({comments}); }
      if (suggestions != null) { this.setState({suggestions}); }
      if (chat != null) { this.setState({chat}); }
    });
    const authorityState = await authority.getState();
    this.setState({
      comments: authorityState.comments,
      suggestions: authorityState.suggestions,
      chat: authorityState.chat,
    });
  }

  componentWillUnmount() { this._unmount(); }

  render({className, authority, ...others}, {comments}) {
    const classNames = ["document-annotations", "panel", className || ""].join(" ");
    return (
        <div className={classNames} {...others}>
          <div className="panel-nav">
            <ul className="tab tab-block">
              <li className="tab-item active">
                <a href="#">Comments</a>
              </li>
              <li className="tab-item">
                <a href="#">Suggestions</a>
              </li>
              <li className="tab-item">
                <a href="#">Chat <span className={"badge"}></span></a>
              </li>
            </ul>
          </div>
          <div className="panel-body oy-initial">
            {comments.map((comment, i) => <Comment key={i} comment={comment}/>)}
            <div className={"tile"}>
              <div className={"tile-icon"}>
                <figure className="avatar">
                  <img src="https://www.gravatar.com/avatar/bfd1c0a0804aa69dcc8c981e42dab725?d=wavatar"
                       alt="Brosenne"/>
                </figure>
              </div>
              <NewCommentForm authority={authority} className="tile-content"/>
            </div>
          </div>
        </div>
    );
  }

}
