import "prosemirror-view/style/prosemirror.css";
import "./style.scss";

import {Component} from "preact";
import PropTypes from "prop-types";

import Editor from "./editor";

/*==================================================== Component  ====================================================*/

export default class DocumentEditor extends Component {

  static propTypes = {
    authority: PropTypes.any.isRequired,
  };

  render({authority, ...props}) {
    return (
        <div {...props}>
          <ul className="tab tab-block mb-0">
            <li className="tab-item active">
              <a href="#">
                Source
              </a>
            </li>
            <li className="tab-item">
              <a href="#">
                Preview
              </a>
            </li>
          </ul>
          <Editor authority={authority} id="editor-wrapper"/>
        </div>
    );
  }

}
